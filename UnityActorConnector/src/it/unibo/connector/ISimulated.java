package it.unibo.connector;

public interface ISimulated 
{
	// Properties
	IConnector getConnector();
	void setConnector(IConnector connector);

}
